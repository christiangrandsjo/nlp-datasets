import pandas as pd


def convert_bias_score(bs):
    if bs == -4:
        return 0
    if bs == -3:
        return 0
    if bs == -2:
        return 0
    if bs == -1:
        return 0
    if bs == 0:
        return 0
    if bs == 1:
        return 1
    if bs == 2:
        return 1
    if bs == 3:
        return 1
    if bs == 4:
        return 1
    return 0


def convert_bias_score2(bs):
    if bs == -4:
        return 0
    if bs == -3:
        return 0.12
    if bs == -2:
        return 0.25
    if bs == -1:
        return 0.375
    if bs == 0:
        return 0.5
    if bs == 1:
        return 0.625
    if bs == 2:
        return 0.75
    if bs == 3:
        return 0.875
    if bs == 4:
        return 1
    return 0


def convert_bias_score3(bs):
    if bs == -4:
        return 0
    if bs == 4:
        return 1


def convert_truthfulness(truthfulness):
    if truthfulness == "true":
        return 1
    if truthfulness == "mostly-true":
        return 1
    if truthfulness == "half-true":
        return 1
    if truthfulness == "barely-true":
        return 0
    if truthfulness == "false":
        return 0
    if truthfulness == "pants-fire":
        return 0
    return 0


def convert_truthfulness2(truthfulness):
    if truthfulness == "true":
        return 1
    if truthfulness == "mostly-true":
        return 0.8
    if truthfulness == "half-true":
        return 0.6
    if truthfulness == "barely-true":
        return 0.4
    if truthfulness == "false":
        return 0.2
    if truthfulness == "pants-fire":
        return 0
    return 0


def convert_truthfulness3(truthfulness):
    if truthfulness == "true":
        return 1

    if truthfulness == "pants-fire":
        return 0


def normalize_dataset(input_file, columns, output_file):
    df = pd.read_csv(input_file, sep="\t", encoding="utf8", usecols=columns, header=0, index_col=None)
    df_out = pd.DataFrame(columns=columns)

    for _, row in df.iterrows():
        for column in columns:
            if column == "bias_score":
                row[column] = convert_bias_score3(row[column])
            if column == "truthfulness":
                row[column] = convert_truthfulness3(row[column])
        df_out = df_out.append(row, ignore_index=True)
    df_out.to_csv(output_file, index=False, sep="\t")


# main
if __name__ == "__main__":
    normalize_dataset("liar/train_bias_700.csv", ["bias_score", "sentence"], "liar/train_bias_700_n3.csv")
    normalize_dataset("liar/train_bias_1000.csv", ["bias_score", "sentence"], "liar/train_bias_1000_n3.csv")
    normalize_dataset("liar/train.csv", ["truthfulness", "sentence"], "liar/train_n3.csv")
    normalize_dataset("liar/test.csv", ["truthfulness", "sentence"], "liar/test_n3.csv")
    normalize_dataset("liar/valid.csv", ["truthfulness", "sentence"], "liar/valid_n3.csv")
